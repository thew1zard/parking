package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Parking {

    private ArrayList<Car> passengerCarPlaces;
    private ArrayList<Car> truckPlaces;

    public ArrayList<Car> getTruckPlaces() {
        return truckPlaces;
    }

    public Parking(int numberOfPassengerCarPlaces, int numberOfTruckPlaces) {
        this.passengerCarPlaces = new ArrayList<>(Collections.nCopies(numberOfPassengerCarPlaces, null));
        this.truckPlaces = new ArrayList<>(Collections.nCopies(numberOfTruckPlaces,null));
        this.passengerCarPlaces.trimToSize();
        this.truckPlaces.trimToSize();
    }


    public ArrayList<Car> getPassengerCarPlaces() {
        return passengerCarPlaces;
    }

    public void setPassengerCarPlaces(ArrayList<Car> passengerCarPlaces) {
        this.passengerCarPlaces = passengerCarPlaces;
    }

    @Override
    public String toString() {
        return "Места на парковке легковых машин: " + passengerCarPlaces +
                "\nМеста на парковке грузовых машин: " + truckPlaces;
    }
}
