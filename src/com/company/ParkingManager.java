package com.company;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;


public class ParkingManager {
    Parking parking;
    Random rand = new Random();

    private boolean isFoundEmptyPassengerCarPlaces;
    private boolean isFoundEmptyTruckPlaces;

    private int uniqueIdForPasCars = 0;
    private int uniqueIdForTrucks = 0;

    public ParkingManager(Parking argParking) {
        this.parking = argParking;
    }


    private int counterOfRemovedCars;


    public void fillingParkingPlacesWithNewCars() {

        System.out.println("Программа заполнения мест на парковки активирована!");

        ArrayList<Car> passengerCars = new ArrayList<>();


        ArrayList<Car> trucks = new ArrayList<>();


        Car currentCar;
        for (int car = 0; car < Math.abs(rand.nextInt((parking.getPassengerCarPlaces().size() +
                parking.getTruckPlaces().size()) / 3)); car++) {

            uniqueIdForPasCars += 1;
            currentCar = new Car("passengerCar", uniqueIdForPasCars);
            passengerCars.add(currentCar);
        }
        for (int car = 0; car < Math.abs(rand.nextInt((parking.getTruckPlaces().size() +
                parking.getPassengerCarPlaces().size()) / 3)); car++) {

            uniqueIdForTrucks += 1;
            currentCar = new Car("truck", uniqueIdForTrucks);
            trucks.add(currentCar);
        }

        System.out.println("Приехало " + passengerCars.size() + " легковых и " + trucks.size() + " грузовых.  Пробую заполнить места...");

        fillingPassengerCarPlaces(passengerCars);
        fillingTruckPlaces(trucks);

    }

    public void waitingTime() {
        int minPasTime = 10;
        int minTruckTime = 10;
        if (parking.getPassengerCarPlaces().stream().noneMatch(Objects::nonNull)) {
            System.out.println("Все места на парковке для легковых машин свободны.");
        } else {
            for (int car = 0; car < parking.getPassengerCarPlaces().size(); car++) {
                if (parking.getPassengerCarPlaces().get(car) != null && minPasTime > parking.getPassengerCarPlaces().get(car).getTime()) {
                    minPasTime = parking.getPassengerCarPlaces().get(car).getTime();
                }
            }
            System.out.println("Оставшееся время до освобождения парковки для легковых машин: " + minPasTime + " ходов.");
        }

        if (parking.getTruckPlaces().stream().noneMatch(Objects::nonNull)) {
            System.out.println("Все места на парковке для грузовых машин свободны.");
        } else {
            for (int car = 0; car < parking.getTruckPlaces().size(); car++) {
                if (parking.getTruckPlaces().get(car) != null && minTruckTime > parking.getTruckPlaces().get(car).getTime()) {
                    minTruckTime = parking.getTruckPlaces().get(car).getTime();
                }
            }
            System.out.println("Оставшееся время до освобождения парковки для грузовых машин: " + minTruckTime + " ходов.");
        }
    }

    public void showInfoAboutPlaces() {
        System.out.println(this.parking + "\n");
    }

    public int countingEmptyPassengerCarPlaces() {

        int counterOfEmptyPlaces = 0;
        if(!parking.getPassengerCarPlaces().contains(null)){
            return 0;
        } else {
            for (Object car : parking.getPassengerCarPlaces()) {
                if (car == null) {
                    counterOfEmptyPlaces += 1;
                }
            }
            return counterOfEmptyPlaces;
        }
    }

    public int countingEmptyTruckPlaces() {
        int counterOfEmptyPlaces = 0;
        for (Object car : parking.getTruckPlaces()) {
            if (car == null) {
                counterOfEmptyPlaces += 1;
            }
        }
        return counterOfEmptyPlaces;
    }



    public void decreaseMove() {
        this.counterOfRemovedCars = 0;
        int counterOfRemovedTrucks = 0;
        int counterOfRemovedPassCars = 0;

        for (int car = 0; car < parking.getPassengerCarPlaces().size(); car++) {
            if (parking.getPassengerCarPlaces().get(car) != null && parking.getPassengerCarPlaces().get(car).getTypeOfCar().equals("truck")) {
                parking.getPassengerCarPlaces().get(car).setTime(parking.getPassengerCarPlaces().get(car).getTime() - 1);
                if (parking.getPassengerCarPlaces().get(car).getTime() == 0) {
                    parking.getPassengerCarPlaces().set(car, null);
                    parking.getPassengerCarPlaces().set(car + 1, null);
                    this.counterOfRemovedCars += 1;
                    counterOfRemovedPassCars += 1;
                }
                car += 1;
            } else if (parking.getPassengerCarPlaces().get(car) != null && parking.getPassengerCarPlaces().get(car).getTypeOfCar().equals("passengerCar")) {
                parking.getPassengerCarPlaces().get(car).setTime(parking.getPassengerCarPlaces().get(car).getTime() - 1);
                if (parking.getPassengerCarPlaces().get(car).getTime() == 0) {
                    parking.getPassengerCarPlaces().set(car, null);
                    this.counterOfRemovedCars += 1;
                    counterOfRemovedPassCars += 1;
                }
            }
        }

        for (int car = 0; car < parking.getTruckPlaces().size(); car++) {
            if (parking.getTruckPlaces().get(car) != null) {
                parking.getTruckPlaces().get(car).setTime(parking.getTruckPlaces().get(car).getTime() - 1);
                if (parking.getTruckPlaces().get(car).getTime() == 0) {
                    parking.getTruckPlaces().set(car,null);
                    this.counterOfRemovedCars += 1;
                    counterOfRemovedTrucks += 1;
                }
            }
        }

        System.out.println("Освободилось " + this.counterOfRemovedCars + " мест.\n   ");
        System.out.println(counterOfRemovedPassCars + " на парковке для легковых машин, " + counterOfRemovedTrucks + " на парковке для грузовых машин.");
    }


    public void resetPlaces() {
        int localPasCounter = countingEmptyPassengerCarPlaces();
        int localTrCounter = countingEmptyTruckPlaces();

        for (int car = 0; car < parking.getPassengerCarPlaces().size(); car++) {
            parking.getPassengerCarPlaces().set(car,null);

        }

        for (int car = 0; car < parking.getTruckPlaces().size(); car++) {
            parking.getTruckPlaces().set(car,null);

        }


        System.out.println("Освобождаю места...");
        System.out.println("Сделано! Освобождено " +
                (parking.getPassengerCarPlaces().size()
                        - localPasCounter) + " мест на парковке для легковых машин, "
                + (parking.getTruckPlaces().size() - localTrCounter)
                + " мест на парковке для грузовых машин.");
    }

    private void fillingTruckPlaces(ArrayList<Car> trucks) {
        int counterOfEmptyPlaces = countingEmptyTruckPlaces();
        int counterOfNotFitTrucks = trucks.size() - counterOfEmptyPlaces;
        if (parking.getTruckPlaces().contains(null)) {
            if (counterOfEmptyPlaces >= trucks.size()) {
                for (Car truck : trucks) {
                    for (int place = 0; place < parking.getTruckPlaces().size(); place++) {
                        if (parking.getTruckPlaces().get(place) == null) {
                            parking.getTruckPlaces().set(place, truck);
                            break;
                        }
                    }
                }
                System.out.println("Отлично, места на грузовой парковке заполнены!");
            } else {
                for (int car = 0; car < counterOfEmptyPlaces; car++) {
                    for (int place = 0; place < parking.getTruckPlaces().size(); place++) {
                        if (parking.getTruckPlaces().get(place) == null) {
                            parking.getTruckPlaces().set(place, trucks.get(car));
                            break;
                        }
                    }
                    trucks.set(car,null);
                }
                System.out.println("На парковку для грузовых мшаин смогло въехать только " + counterOfEmptyPlaces +
                        " грузовых машин. Остальные должны либо покинуть парковку," +
                        " либо заехать на парковку для легковых, если там имеется место");

                int counterOfTwoConsecutiveNullPlaces = countingTwoConsecutiveNullPlaces();
                if (counterOfTwoConsecutiveNullPlaces >= counterOfNotFitTrucks) {
                    fillingPassPlacesWithTrucks(trucks);
                    System.out.println("На парковку для легковых машин въехало " + counterOfTwoConsecutiveNullPlaces
                            + " грузовиков. Остальные вынуждены покинуть парковку");
                }

            }
        } else {
            int counterOfTwoConsecutiveNullPlaces = countingTwoConsecutiveNullPlaces();
            if (counterOfTwoConsecutiveNullPlaces >= counterOfNotFitTrucks) {
                fillingPassPlacesWithTrucks(trucks);
            } else {
                System.out.println("Мест для прибовших грузовых машин ни на одной парковке нет. Им придется уехать :( ");
            }
        }
    }

    private void fillingPassPlacesWithTrucks(ArrayList<Car> trucks) {
        for (int car = 0; car < trucks.size(); car++) {
            if (trucks.get(car) != null) {
                for (int place = 0; place < parking.getPassengerCarPlaces().size() - 1; place++) {
                    if (parking.getPassengerCarPlaces().get(place) == null && parking.getPassengerCarPlaces().get(place + 1) == null) {
                        parking.getPassengerCarPlaces().set(place,trucks.get(car));
                        parking.getPassengerCarPlaces().set(place + 1, trucks.get(car));
                        break;
                    }
                }
            }
        }
    }

    private void fillingPassengerCarPlaces(ArrayList<Car> passengerCars) {

        int counterOfEmptyPlaces = countingEmptyPassengerCarPlaces();
        if (parking.getPassengerCarPlaces().contains(null)) {
            if (counterOfEmptyPlaces >= passengerCars.size()) {
                for (Car passCar : passengerCars) {
                    for (int place = 0; place < parking.getPassengerCarPlaces().size(); place++) {
                        if (parking.getPassengerCarPlaces().get(place) == null) {
                            parking.getPassengerCarPlaces().set(place,passCar);
                            break;
                        }
                    }
                }
                System.out.println("Отлично, места заполнены!");
            } else {
                for (int car = 0; car < counterOfEmptyPlaces; car++) {
                    for (int place = 0; place < parking.getPassengerCarPlaces().size(); place++) {
                        if (parking.getPassengerCarPlaces().get(place) == null) {
                            parking.getPassengerCarPlaces().set(place, passengerCars.get(car));
                            break;
                        }
                    }
                }
                System.out.println("На парковку для легковых машин могло въехать только " + counterOfEmptyPlaces +
                        " легковых машин. Остальные вынуждены покинуть парковку.");
            }
        } else {
            System.out.println("Свободных мест нет. Вновь прибывшим легковым транспортным средствам придется покинуть парковку :(");
        }
    }

    private int countingTwoConsecutiveNullPlaces() {
        int counterOfTwoConsecutiveNullPlaces = 0;
        for (int i = 0; i < parking.getPassengerCarPlaces().size() - 1; i++) {
            if (parking.getPassengerCarPlaces().get(i) == null && parking.getPassengerCarPlaces().get(i + 1) == null) {
                counterOfTwoConsecutiveNullPlaces += 1;
            }
        }
        return counterOfTwoConsecutiveNullPlaces;
    }
}
