package com.company;

import java.util.Random;

public class Car {
    private int uniqueId;
    private int time;

    private String typeOfCar;

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }


    @Override
    public String toString() {

        String uniqueIDStr = String.format("%04d", uniqueId);
        return "Машина{" +
                "ID=" + uniqueIDStr +
                ", оставшиеся ходы=" + time +
                ", тип машины='" + typeOfCar + '\'' +
                '}';
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    public Car(String typeOfCar, int uniqueId) {
        Random random = new Random();
        this.uniqueId = uniqueId;

        this.time = Math.abs(random.nextInt(10)) + 1;
        this.typeOfCar = typeOfCar;
    }


}