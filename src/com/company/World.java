package com.company;

import java.util.Scanner;


public class World {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean isInput = true;
        int truckNumber = 0;
        int passNumber = 0;
        System.out.print("Введите размер парковки для легковых машин: ");
        while (isInput) {
            passNumber = sc.nextInt();
            if (passNumber <= 0) {
                System.out.println("Пожалуйста, введите число больше 0.");
            } else {
                isInput = false;
            }
        }
        isInput = true;
        System.out.print("Введите размер парковки для грузовых машин: ");
        while (isInput) {
            truckNumber = sc.nextInt();
            if (truckNumber <= 0) {
                System.out.println("Пожалуйста, введите число больше 0.");
            } else {
                isInput = false;
            }
        }

        System.out.print("Введите размер парковки для грузовых машин: ");
        Parking myParking = new Parking(passNumber, truckNumber);
        ParkingManager myParkingManager = new ParkingManager(myParking);
        boolean isWorks = true;
        while (isWorks) {
            isInput = true;

            myParkingManager.fillingParkingPlacesWithNewCars();
            while (isInput) {
                System.out.print("\nВведите команду (доступные команды: 'remainingPlaces' - узнать количество свободных мест" +
                        ", 'occupiedPlaces' - узнать количество занятых мест, \n'waitingTime' - время ожидания места," +
                        " 'goNext' - переход к следующему ходу," +
                        "'showInfo' - информация по местам на парковках, 'quit' - выход) :  ");
                String keyboardInput = sc.next();
                switch (keyboardInput) {
                    case "remainingPlaces":
                        System.out.println("Количество оставшихся мест на парковке для легковых машин: " + myParkingManager.countingEmptyPassengerCarPlaces());
                        System.out.println("Количнство оставшихся мест на парковке для грузовых машин: " + myParkingManager.countingEmptyTruckPlaces());
                        break;
                    case "occupiedPlaces":
                        System.out.println("Количество занятых мест на парковке для легковых машин: " +
                                (myParkingManager.parking.getPassengerCarPlaces().size() - myParkingManager.countingEmptyPassengerCarPlaces()));
                        System.out.println("Количество занятых мест на парковке для грузовых машин: " + (myParkingManager.parking.getTruckPlaces().size() - myParkingManager.countingEmptyTruckPlaces()));
                        break;
                    case "waitingTime":
                        myParkingManager.waitingTime();
                        break;
                    case "goNext":
                        System.out.println("Идем дальше!\n");
                        myParkingManager.decreaseMove();
                        isInput = false;
                        break;
                    case "quit":
                        isWorks = false;
                        isInput = false;
                        System.out.println("До скорых встреч!");
                        break;
                    case "resetPlaces":
                        myParkingManager.resetPlaces();
                        break;
                    case "showInfo":
                        myParkingManager.showInfoAboutPlaces();
                        break;

                    default:
                        System.out.println("Неизвестная команда! Пожалуйста попробуйте еще раз. Доступные команды:" +
                                " 'remainingPlaces' - узнать количество" +
                                "\n свободных мест, 'occupiedPlaces' - узнать количество занятых мест, 'waitingTime' " +
                                "\n- время ожидания места, 'goNext' - переход к следующему ходу," +
                                ", 'showInfo' - информация по местам на парковках, 'quit' - выйти из программы) ");
                        break;
                }

            }
        }
    }

}
